Software Dependencies:-
####################################################################
(A) SQL Server

(B) Nodejs (current version )

(C) Postman



Configure the project in the steps mentioned below:
###################################################################

(A) SQL Server
--------------------------------------------------

1) Install SQL Server
Download "SQL Server 2019 Express" from below link:
https://www.microsoft.com/en-in/sql-server/sql-server-downloads

Install and configure SQL Server and run the SQL server.

2) Run the "Microsoft SQL Server Managemet Studio"

3) Create a "user" after logging into SQL 

   Security -> Login -> right click and create users and give permissions.
 
4) Update the below details in "Connection.js" in connection folder with SQL details of particular system

        user: ':  //Update this
        
        password:   //Update this
        
        server :   //Update this
        
        database:  //Update this


5) Create a database with name "TESTDB1"

6) Create a "Products" table with fields ProductID, ProductName, ProductPrice.

7) Insert some records into "Products" table


(B) NodeJS
--------------------------------------------------
1. Install nodejs from : https://nodejs.org/en/download/

2. Now go to project folder and type the following command
npm install

3. Then run the backend server by typing
node server.js


(C) Postman
--------------------------------------------------
1. Open "Postman"

2. Paste the below URL in the postman with "Get" method 
 http://localhost:1337/api/products

We can view the "Products" records.
